
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Configuration
 * Credits
 
INTRODUCTION
------------
Administer Anonymous Comments (https://www.drupal.org/project/adminanoncomment)
extends Drupal's comment module to Allows administrators to post and edit comments as anonymous users (that is, not assigning to a user account) and also to post comments on nodes with comments disabled.

INSTALLATION
------------

1. Copy the entire adminanoncomment project directory (not just the contents) to your 
   normal module directory (ie: sites/all/modules)
   
2. Enable the Administer Anonymous Comments module at ?q=admin/build/modules

CONFIGURATION
-------------
   
1. Configure "Administer anonymous comments" settings at
    admin/config/content/adminanoncomment 
    * .
    * Topics per page: Up to you.
    * Default order: "Date - newest first" so the most recent posts are at the top of the 
      topic list.  
    
CREDITS
-------
Developer and maintainer: Benjamin Melançon ( https://www.drupal.org/u/mlncn )
of Agaric, a worker-owned web development collective ( http://agaric.com )
